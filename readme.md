# Romashka Arkanoid

It's just another HTML5 arkanoid clone written for fun and learning purposes. The girl in the picture is my friend Romashka ('Chamomile' in Ukrainian). Game asserts has free domain license.

![Screenshot](./screenshot.jpg)

The game is controlled by mouse and keyboard. 

* Mouse click on plaftorm surface — kick ball or stop platform
* Mouse click to the left of the platform — change platform direction to left
* Mouse click to the right of the platform — change platform direction to right
* Left/right arrow keys — change platform direction
* Space — kick ball or stop platform

**[Play game online](https://palma3000.itch.io/romashka_arkanoid)**

## License

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2024 Palma3000 <palma3000@airmail.cc>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
```


